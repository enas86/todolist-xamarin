﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace ToDoList
{
    [Table("Tasks")]
    class TaskModel : INotifyPropertyChanged
    {
        private int id;
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id
        { 
            get { return id; }
            set { id = value; }
        }

        private string task;
        public string Task
        {
            get { return task; }
            set { task = value; }
        }

        private States state;
        public States State
        {
            get { return state; }
            set 
            {
                state = value;
                OnPropertyChanged("State");
            }
        }

        public TaskModel()
        {
            Task = "";
        }
        public TaskModel(string task)
        {
            Task = task;
            this.state = States.InProgress;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
