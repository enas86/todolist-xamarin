﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ToDoList
{
    public enum States { InProgress, Done };
    class ApplicationViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<TaskModel> Tasks { get; set; }
        private SQLiteConnection database;

        private string newTaskText;
        public string NewTaskText
        {
            get { return newTaskText; }
            set 
            {
                newTaskText = value;

                OnPropertyChanged("NewTaskText");
                (add as Command).ChangeCanExecute();
            }
        }

        public ICommand add { get; set; }
        public ICommand delete { get; set; }
        public ICommand update { get; set; }

        public ApplicationViewModel()
        {
            database = new SQLiteConnection(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TasksBase.db"));
            database.CreateTable<TaskModel>();

            Tasks = new ObservableCollection<TaskModel>();
            var tempTasks = database.Table<TaskModel>().ToList();
            foreach (TaskModel temp in tempTasks)
                Tasks.Add(temp);

            add = new Command(
                execute: () =>
                {
                    TaskModel task = new TaskModel(NewTaskText as string);
                    database.Insert(task);

                    var id = database.Query<TaskModel>("SELECT * FROM Tasks WHERE _id = (SELECT MAX(_id) FROM Tasks)");
                    foreach (var result in id)
                        task.Id = result.Id;
                    
                    Tasks.Add(task);

                    NewTaskText = "";
                },
                canExecute: () =>
                {
                    return NewTaskText != "" && NewTaskText.Length < 30;
                });
            delete = new Command(
                execute: (obj) =>
                {
                    TaskModel task = obj as TaskModel;
                    database.Delete<TaskModel>(task.Id);

                    Tasks.Remove(task);
                },
                canExecute: (obj) =>
                {
                    return obj != null;
                });
            update = new Command(
                execute: (obj) =>
                {
                    TaskModel task = obj as TaskModel;
                    Tasks[Tasks.IndexOf(task)].State = task.State == States.InProgress ? States.Done : States.InProgress;
                    database.Update(Tasks[Tasks.IndexOf(task)]);
                },
                canExecute: (obj) =>
                {
                    return true;
                });

            NewTaskText = "";
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
